/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,js,ts,jsx,tsx}", // Chemins vers les fichiers où Tailwind CSS est utilisé
    "./public/index.html",
  ],
  theme: {
    extend: {
      colors: {
        'purple': '#AE147D',
        'pink': '#B02580',
      },
      opacity: {
        '6': '0.6',
      },
    },
  },
  plugins: [],
}
