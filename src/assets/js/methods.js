import axios from 'axios';
import { isArray } from "chart.js/helpers";

function getCookie(name) {
    const nameEQ = name + "=";
    const ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
      let c = ca[i].trim();
      if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length);
    }
    return null;
  }

export const obtainDataContenu = async (loadingTwo, error, events, carrousels) => {
    loadingTwo.value = true;
    error.value = [];
  
    try {
      const [responseEvents, responseCarrousels] = await Promise.all([
        axios.get(process.env.VUE_APP_API_URL + 'events', {
          headers: { Authorization: 'Bearer ' + getCookie('accessToken') }
        }),
        axios.get(process.env.VUE_APP_API_URL + 'carrousels', {
          headers: { Authorization: 'Bearer ' + getCookie('accessToken') }
        })
      ]);
  
      if (responseEvents.status === 200 && responseCarrousels.status === 200) {
        events.items = responseEvents.data;
        carrousels.items = responseCarrousels.data;
      }
    } catch (err) {
      if (err.response && err.response.status >= 400) {
        if (isArray(err.response.data.message)) {
          err.response.data.message.forEach(element => {
            error.value.push(element.error || 'An error occurred');
          });
        } else {
          error.value.push(err.response.data.message.error || 'An error occurred');
        }
       
      } else {
        error.value.push('An unexpected error occurred');
      }
    } finally {
      loadingTwo.value = false;
    }
  };