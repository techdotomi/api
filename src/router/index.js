import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Tables from "../views/Tables.vue";
import Billing from "../views/Billing.vue";
import VirtualReality from "../views/VirtualReality.vue";
import RTL from "../views/Rtl.vue";
import Profile from "../views/Profile.vue";
import Signin from "../views/Signin.vue";
import Utilisateurs from "../views/Utilisateurs.vue";
import Articles from "../views/Articles.vue";
import Contenu from "../views/Contenu.vue";
import Messages from "../views/Messages.vue";
import Packages from "../views/Packages.vue";
import ArticlesDetails from "../views/ArticlesDetails.vue";
import UserDetails from "../views/UserDetails.vue";
import Abonnements from "../views/Abonnements.vue";
import Aggregateurs from "../views/Aggregateurs.vue";
import Transactions from "../views/Transactions.vue";
import PackagesDetails from "../views/PackagesDetails.vue";
import NotFound from "../views/NotFound.vue";

const routes = [
  {
    path: "/",
    name: "/",
    redirect: "/signin",
  },
  {
    path: "/notfound",
    name: "NotFound",
    component: NotFound,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
  },
  {
    path: "/tables",
    name: "Tables",
    component: Tables,
  },
  {
    path: "/billing",
    name: "Billing",
    component: Billing,
  },
  {
    path: "/virtual-reality",
    name: "Virtual Reality",
    component: VirtualReality,
  },
  {
    path: "/rtl-page",
    name: "RTL",
    component: RTL,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/user/:id",
    name: "UserDetails",
    component: UserDetails,
  },
  {
    path: "/packages/:id",
    name: "PackagesDetails",
    component: PackagesDetails,
  },
  {
    path: "/articles/:id",
    name: "ArticlesDetails",
    component: ArticlesDetails,
  },
  {
    path: "/signin",
    name: "Signin",
    component: Signin,
  },
  {
    path: "/utilisateurs",
    name: "utilisateurs",
    component: Utilisateurs,
  },
  {
    path: "/articles",
    name: "articles",
    component: Articles,
  },
  {
    path: "/contenu",
    name: "contenu",
    component: Contenu,
  },
  {
    path: "/messages",
    name: "messages",
    component: Messages,
  },
  {
    path: "/packages",
    name: "packages",
    component: Packages,
  },
  {
    path: "/abonnements",
    name: "abonnements",
    component: Abonnements,
  },
  {
    path: "/aggregateurs",
    name: "aggregateurs",
    component: Aggregateurs,
  },
  {
    path: "/transactions",
    name: "transactions",
    component: Transactions,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  linkActiveClass: "active",
});

router.beforeEach((to, from, next) => {
  const userData = JSON.parse(sessionStorage.getItem("user")); // Récupération de l'utilisateur
  const roleData = JSON.parse(sessionStorage.getItem("role"));
  const isAuthenticated = !!userData; // Vérifie si l'utilisateur est connecté
  
  // 📌 Définition des rôles et accès
  const authRoutes = ["/dashboard", "/profile", "/articles", "/contenu"];
  const adminRoutes = ["/utilisateurs", "/abonnements", "/tables", "/billing", "/messages", "/aggregateurs", "/transactions", "/packages"]; // Accès réservé aux admins

  if (!isAuthenticated && authRoutes.includes(to.path)) {
    next("/signin"); // Redirige les non-connectés vers la page de connexion
  } else if (adminRoutes.includes(to.path) && roleData.name !== "Admin") {
    next("/notfound"); // Redirige vers le dashboard si l'utilisateur n'est pas admin
  } else {
    next(); // Continue normalement
  }
});


export default router;
